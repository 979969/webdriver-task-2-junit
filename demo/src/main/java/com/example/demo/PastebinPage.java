package com.example.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public class PastebinPage {

	private WebDriver driver;

	private final String URL = "https://pastebin.com/";

	private By pasteCodeField = By.id("postform-text");
	private By syntaxHighlightingDropdown = By.id("select2-postform-format-container");
	private By expirationDropdown = By.id("select2-postform-expiration-container");
	private By pasteNameField = By.id("postform-name");
	private By createButton = By.xpath("//button[@class='btn -big']");

	public PastebinPage(WebDriver driver) {
		this.driver = driver;
	}

	public void open() {
		driver.get(URL);
	}

	public void fillPasteForm(String code, String expiration, String name) {
		WebElement pasteCodeFieldElement = driver.findElement(pasteCodeField);
		pasteCodeFieldElement.sendKeys(code);

		WebElement expirationDropdownElement = driver.findElement(expirationDropdown);
		expirationDropdownElement.click();
		WebElement expirationOption = driver.findElement(By.xpath("//li[text()='" + expiration + "']"));
		expirationOption.click();

		WebElement pasteNameFieldElement = driver.findElement(pasteNameField);
		pasteNameFieldElement.sendKeys(name);
	}

	public void selectSyntaxHighlighting(String syntax) {
		WebElement syntaxHighlightingDropdownElement = driver.findElement(syntaxHighlightingDropdown);
		syntaxHighlightingDropdownElement.click();
		WebElement syntaxOption = driver.findElement(By.xpath("//li[text()='" + syntax + "']"));
		syntaxOption.click();
	}

	public void selectExpiration(String expiration) {
		WebElement expirationDropdownElement = driver.findElement(expirationDropdown);
		expirationDropdownElement.click();
		WebElement expirationOption = driver.findElement(By.xpath("//li[text()='" + expiration + "']"));
		expirationOption.click();
	}

	public void createPaste() {
		WebElement createButtonElement = driver.findElement(createButton);
		createButtonElement.click();
	}

	public String getPasteTitle() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		wait.until(ExpectedConditions.titleContains("how to gain dominance among developers"));
		return driver.getTitle();
	}

	public boolean isSyntaxHighlightedAs(String syntax) {
		WebElement syntaxButton = new WebDriverWait(driver, Duration.ofSeconds(10))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@href='/archive/" + syntax.toLowerCase() + "']")));
		return syntaxButton.isDisplayed();
	}

	public boolean doesCodeMatchExpectedText(String expectedText) {
		String actualText = driver.findElement(By.cssSelector("div.source.bash")).getText();
//		System.out.println("Actual text is " + actualText);
//		System.out.println("Expected text is " + expectedText);
		return actualText.equals(expectedText);
	}
	public void clickAgreeButton() {
		try {
			WebElement agreeButton = new WebDriverWait(driver, Duration.ofSeconds(5))
					.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button[class=' css-47sehv']")));
			agreeButton.click();
		} catch (TimeoutException e) {
			System.out.println("Agree button didn't appear within the specified wait time.");
		}
	}
	public void clickCloseBanner() {
		try {
			Thread.sleep(5000); // Wait for 5 seconds

			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(7));
			WebElement banner = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("hideSlideBanner")));

			if (banner.isDisplayed()) {
				banner.click();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			System.out.println("Banner downside didn't appear within the specified wait time.");
		}
	}
}
