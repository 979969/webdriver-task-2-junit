package com.example.demo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PastebinTest {

	private WebDriver driver;
	private PastebinPage pastebinPage;

	@BeforeEach
	public void setUp() {
		driver = new ChromeDriver();
		pastebinPage = new PastebinPage(driver);
	}

	@Test
	public void verifyNameSyntaxCode() {
		pastebinPage.open();

		// Additional clicks for privacy pop-up (Agree) and footer banner (Close X)
		pastebinPage.clickAgreeButton();
		pastebinPage.clickCloseBanner();

		pastebinPage.fillPasteForm("git config --global user.name  \"New Sheriff in Town\"\n" +
				"git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
				"git push origin master --force", "10 Minutes", "how to gain dominance among developers");

		// Choose syntax highlighting
		pastebinPage.selectSyntaxHighlighting("Bash");

		// Choose expiration time
		pastebinPage.selectExpiration("10 Minutes");

		// Click the create button
		pastebinPage.createPaste();

		// Assert paste details after creating the paste
		assertEquals("how to gain dominance among developers - Pastebin.com", pastebinPage.getPasteTitle());
		assertTrue(pastebinPage.isSyntaxHighlightedAs("Bash"));
		assertTrue(pastebinPage.doesCodeMatchExpectedText("git config --global user.name  \"New Sheriff in Town\"\n" +
				"git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
				"git push origin master --force"));
	}

	@AfterEach
	public void tearDown() {
		if (driver != null) {
			driver.quit();
		}
	}
}
